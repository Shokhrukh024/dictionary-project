import Vue from 'vue'
import VueRouter from 'vue-router'

Vue.use(VueRouter)

const routes = [
  {
    path: '/',
    component: () => import('../layouts/MainLayout.vue'),
    children: [
      {path: '', component: () => import('../views/Home.vue')},
      {name: 'product-details', path: 'product-details/:id', props: true, component: () => import('../views/ProductDetails.vue')},

    ]
  },
  {
    path: '/',
    component: () => import('../layouts/Empty.vue'),
    children: [
      {path: 'login', component: () => import('../views/Login.vue')},
      {path: 'login2', component: () => import('../views/Login2.vue')},
    ]
  },
  {
    path: '/',
    component: () => import('../layouts/DashboardLayout.vue'),
    children: [
      {path: 'dashboard', component: () => import('../views/Dashboard.vue'), meta: {authRequired: true}},
      {path: 'new-word', component: () => import('../views/NewWord.vue'), meta: {authRequired: true}},
      {path: 'new-category', component: () => import('../views/NewCategory.vue'), meta: {authRequired: true}},
      {path: 'edit-word', component: () => import('../views/EditWord.vue'), meta: {authRequired: true}},
      
    ]
  },
]

const router = new VueRouter({
  mode: 'history',
  base: process.env.BASE_URL,
  routes
})

export default router
