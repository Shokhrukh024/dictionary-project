import Vue from 'vue'
import Vuex from 'vuex'
import User from './user'
import Word from './word'
import Category from './category'


Vue.use(Vuex)

export default new Vuex.Store({
  modules: {
    User,
    Word,
    Category
  }
})
