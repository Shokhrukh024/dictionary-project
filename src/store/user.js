import axios from 'axios';
import router from '../router/index'

var baseUrl = 'http://localhost:5000/';

export default {
  state: {
      user: {token: ''}
  },
  mutations: {
    SET_USER_TO_STATE: (state, payload) => {
        state.user = payload
    },
    UNSET_USER_FROM_STATE: (state) => {
        state.user = {token: ''};
    },
    LOG_OUT: (state) =>{
       sessionStorage.clear();
       state.user = {token: ''};
    }
  },
  actions: {
      async SIGNIN({commit}, payload) {
        return await axios({
            method: "POST",
            url: baseUrl + `users/login`,
            data: payload,
          })
          .then((e) => {
            commit('SET_USER_TO_STATE', e.data);
            sessionStorage.setItem('user', JSON.stringify(e.data));
            router.push({path: '/dashboard'});
            // return e;
          })
          .catch((error) => {
            console.log(error);
            return 'error';
          })
      },
      STATE_CHANGED({commit}, payload){
          commit('SET_USER_TO_STATE', payload);
      },
      LOGOUT({commit}){
        commit('LOG_OUT');
      }
  },
  getters: {
    getUser: state => state.user
    
  }
  
}




