import axios from 'axios';

var baseUrl = 'http://localhost:5000/';

export default {
  state: {
    categories: []
  },
  mutations: {
    SET_ALL_CATEGORIES: (state, payload) => {
        state.categories = payload
    },
  },
  actions: {
    async ADD_CATEGORY({commit, getters}, payload) {
        return await axios({
            method: "POST",
            url: baseUrl + `categories/`,
            headers: {Authorization: getters.getUser.token},
            data: payload,
          })
          .then((e) => {
            console.log(e.data)
            // return e;
          })
          .catch((error) => {
            console.log(error);
            return 'error';
          })
      },
      async GET_CATEGORIES({commit, getters}) {
        return await axios({
            method: "GET",
            url: baseUrl + `categories/all/`,
            headers: {Authorization: getters.getUser.token}
          })
          .then((e) => {
            commit('SET_ALL_CATEGORIES', e.data)
            // return e;
          })
          .catch((error) => {
            console.log(error);
            return 'error';
          })
      },
  },
  getters: {
    getCategories: state => state.categories
  }
  
}




