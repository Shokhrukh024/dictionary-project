import axios from 'axios';

var baseUrl = 'http://localhost:5000/';

export default {
  state: {
    words: []
  },
  mutations: {
    SET_ALL_WORDS: (state, payload) => {
        state.words = payload
    },
   
  },
  actions: {
      async ADD_WORD({commit, getters}, payload) {
        return await axios({
            method: "POST",
            url: baseUrl + `words/`,
            headers: {Authorization: getters.getUser.token},
            data: payload,
          })
          .then((e) => {
            console.log(e.data)
            // return e;
          })
          .catch((error) => {
            console.log(error);
            return 'error';
          })
      },
      async GET_WORDS({commit, getters}) {
        return await axios({
            method: "GET",
            url: baseUrl + `words/all/`,
            headers: {Authorization: getters.getUser.token}
          })
          .then((e) => {
            commit('SET_ALL_WORDS', e.data)
            // return e;
          })
          .catch((error) => {
            console.log(error);
            return 'error';
          })
      },
  },
  getters: {
    getWords: state => state.words
  }
  
}




